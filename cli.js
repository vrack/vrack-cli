const { Command } = require('commander');
const remote = require('./VRackRemote');
const program = new Command();

program.version('0.0.1');

program
  .option('-d, --debug', 'Включение дебаг режима (нет)')
  .option('-k, --key <key>', 'Ключ доступа')
  .option('-p, --private <private>', 'Приватный ключ')
  .option('-h, --host <host>', 'VRack server host')
  .option('-p, --port <port>', 'VRack server port')
  .option('-c, --command <command>', 'Одна из списка команд')
  .option('-s, --params <params>', 'Параметры команды')
  .option('-l, --list', 'Выводит список доступных команд')
  .option('-w, --wtf <cmd>', 'Выводит информацию о команде')

program.parse(process.argv);

const options = program.opts();
if (options.debug) console.log('Параметры командной строки: ',options);

if (options.key === undefined) options.key = 'default'
if (options.host === undefined) options.host = '127.0.0.1'
if (options.port === undefined) options.port = 4044

const main = async () => {
  // Создаем 
  try {
    const rPath = `ws://${options.host}:${options.port}`;
    // const remote = new VRackRemote(rPath, options.key)
    remote.address = rPath
    remote.key = options.key

    // LIST
    if (options.list){
      console.log('\n Список доступных команд VRack: \n')
      var i = 1
      for (const nm in remote.commands){
        const cmd = remote.commands[nm]
        console.log(` ${i}. ${nm} - ${cmd.description}`)
        i++
      }
      console.log('')
      process.exit(-1)
    }

    // WTF
    if (options.wtf){
      if (!remote.commands[options.wtf]) throw new Error('Команда не найдена')
      console.log('\n Дополнительная информация по команде \n')
      const cmd = remote.commands[options.wtf]
      console.log(` ${options.wtf} - ${cmd.description}`)
      console.log(' Параметры:')
      var i = 1
      for (const pm in cmd.params){
        console.log(`\t ${i}. ${pm} - ${cmd.params[pm]}`)
        i++
      }
      if (i===1) console.log('\t Параметры отсутствуют')
      console.log(``)
      process.exit(-1)
    }

    if (options.command === undefined) throw new Error('Необходимо указать команду')
    if (remote.commandList.indexOf(options.command) === -1) throw new Error('Команда не найдена')
    if (options.private) {
      if (options.key.length !== 16) throw new Error("Не верный размер ключа доступа")
      if (options.private.length !== 32) throw new Error("Не верный размер приватного ключа")
      remote.private = options.private
    }
    var params = {}
    try {
      params = eval(`(${options.params})`)
      if (typeof params !== 'object') throw new Error()
    }catch (error){
      throw new Error("Не валидные параметры команды")
    }

    await remote.promiseConnect()
    await remote.apiKeyAuth(options.key);
    const result = await remote.commandPromise(options.command, {data: params});
    const res = {
      result: result.result,
      resultData: result.resultData
    }
    if (options.debug) {
      console.log('Результат выполнения запроса: ',result)
    }else{
      console.log(JSON.stringify(res))
    }
    
  }catch (error){
    if (options.debug) {
      console.log(error)
    }else{
      console.log(JSON.stringify({result:'error',resultData: error.message}))
    }
  }
  process.exit(-1)
}

main()