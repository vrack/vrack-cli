# VRack CLI

Клиент командной строки для VRack. Позволяет из командной строки отправлять команды для сервиса VRack.

**Экспериментальная версия уже с поддержкой еще не вышедшей VRack 0.3.2**

## Установка

Требуется nodejs версии 12 или выше (требуется поддержка классов)

```
git clone https://gitlab.com/vrack/vrack-cli.git
cd ./vrack-cli
npm install
```

## Быстрый старт

### Получение помощи

```
node ./cli.js --help
```

```
Options:
  -V, --version            output the version number
  -d, --debug              Включение дебаг режима (нет)
  -k, --key <key>          Ключ доступа
  -p, --private <private>  Приватный ключ
  -h, --host <host>        VRack server host
  -p, --port <port>        VRack server port
  -с, --command <command>  Одна из списка команд
  -s, --params <params>    Параметры команды
  -l, --list               Выводит список доступных команд
  -w, --wtf <cmd>          Выводит информацию о команде
  --help                   display help for command
```

### Получения списка доступных команд VRack

```
node ./cli.js -l
```

```
 1. apiKeyList - Возвращает список действующих ключей
 2. apiKeyAdd - Создание нового ключа
 3. apiKeyUpdate - Обновление названия ключа
 4. apiKeyDelete - Удаление ключа
 5. apiKeyIpAdd - Добавление подсети для ключа
 6. apiKeyIpDelete - Удаление подсети для ключа
 7. dashboardRun - Запуск дашборда
 8. dashboardStop - Остановка дашборда
 9. dashboardErrors - Получение критических ошибок
 10. dashboardErrorsClear - Удаление всех критических ошибок
 11. dashboardListUpdate - Обновление списка дашбордов
 12. dashboardList - Получение списка дашбордов
 13. dashboardDeviceAction - Отправка команды устройству
 14. dashboard - Получение данных дашборда
 15. dashboardMeta - Получение мета-данных дашборда
 16. dashboardStructure - Получение структуры дашборда
```

### Получение помощи по конкретной команде VRack

```
node ./cli.js --wtf "dashboardDeviceAction"
```

```
 dashboardDeviceAction - Обновление списка дашбордов
 Параметры:
         1. dashboard - string - Идентификатор дашборда
         2. device - string - Идентификатор устройства
         3. action - string - Action устройства
         4. push - object - Данные для отправки устройству
```

### Отправка команды VRack

Для отправки параметров команды используется js-style объекты в двойных ковычках, строки в которых указываются в ординарных ковычках

```
  {param: 'string', intParam: 1235, booleanParam: true}
```

Пример отправки команды на получения информации о дашборде

```
node ./cli.js -k "1234567890123456" -p "12345678901234567890123456789012" -с "dashboard" -d -s "{dashboard:'test'}"
```

Пример добавления нового ключа доступа

```
node ./cli.js -k "1234567890123456" -p "12345678901234567890123456789012" -с "apiKeyAdd" -d -s "{name:'test',description:'test description',cipher:true,level:1}"
```

## Ответы и ошибки

Ответ формируется в формате JSON и печатается в терминале

```json
{
  "result":"success",
  "resultData":{
      "id":"test",
      "name":"No name",
      ...
  }}
```

Где:

- **result** _string_ - Результат выполнения запроса success/error
- **resultData** _object|string_ - Данные результата выполнения запроса

Если указать параметр дебага то будет выведена информация по принятым параметрам и результатом ответа.

```js
"Параметры командной строки":  {
  key: '39f418817fc57d6c',
  private: 'b9486211c84365ddfd446df806913938',
  debug: true,
  command: 'dashboard',
  params: "{dashboard:'test'}"
}
"Результат выполнения запроса":  {
  data: { dashboard: 'test' },
  command: 'dashboard',
  _pkgIndex: 1002,
  providerID: 'WebSocketServer',
  clientID: 49,
  result: 'success',
  resultData: {
    id: 'test',
    name: 'No name',
    group: 'Default',
    run: false,
    path: 'dashboards/test.json',
    structurePath: 'dashboards/test.struct.json',
    metaPath: 'dashboards/test.meta.json',
    errors: 0,
    deleted: false
  }
}
```
