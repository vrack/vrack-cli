const WebSocket = require('ws');
const crypto = require('crypto')

module.exports = {
  address: 'ws://localhost:4044/',

  _pkgIndex: 1000,
  key: '',
  private: '',
  level: 1000,
  connected: false,
  connection: false,
  disconnected: false,
  channels: new Map(),
  _ws: false,
  _queue: new Map(),
  _queueTimeout: new Map(),
  commandList: [
    'apiKeyList',
    'apiKeyAdd',
    'apiKeyUpdate',
    'apiKeyDelete',
    'apiKeyIpAdd',
    'apiKeyIpDelete',
    'apiKeyRegisteredFilters',
    'apiKeyFilters',
    'apiKeyUpdateFilter',
    'dashboardRun',
    'dashboard',
    'dashboardStop',
    'dashboardErrors',
    'dashboardErrorsClear',
    'dashboardListUpdate',
    'dashboardList',
    'dashboardMeta',
    'dashboardStructure',
  ],

  commands: {
    // API KEY MANAGER
    apiKeyList: {
      description: 'Возвращает список действующих ключей',
      params: {}
    },
    apiKeyAdd: {
      description: 'Создание нового ключа',
      params: {
        name: 'string - Название ключа',
        description: 'string - Описание',
        level: 'integer - Уровень доступа',
        cipher: 'boolean - Использовать шифрование'
      }
    },
    apiKeyUpdate: {
      description: 'Обновление названия ключа',
      params: {
        key: 'string - Ключ',
        name: 'string - Новое название ключа',
        description: 'string - Новое описание',
      }
    },
    apiKeyDelete: {
      description: 'Удаление ключа',
      params: {
        key: 'string - Ключ',
      }
    },
    apiKeyIpAdd: {
      description: 'Добавление подсети для ключа',
      params: {
        key: 'string - Ключ',
        network: 'string - Подсеть ipv4,ipv6 для доступа по ключу ("127.0.0.1/24"...)',
      }
    },
    apiKeyIpDelete: {
      description: 'Удаление подсети для ключа',
      params: {
        key: 'string - Ключ',
        network: 'string - Подсеть ipv4,ipv6 для удаления',
      }
    },
    apiKeyRegisteredFilters: {
      description: 'Возвращает список зарегистрированных фильтров',
      params: {}
    },
    apiKeyUpdateFilter: {
      description: 'Удаление подсети для ключа',
      params: {
        key: 'string - Ключ',
        path: 'string - Путь фильтра',
        list: 'array - Список фильтра',
      }
    },
    // DASHBOARD MANAGER
    dashboardRun: {
      description: 'Запуск дашборда',
      params: {
        dashboard: 'string - Идентификатор дашборда'
      }
    },
    dashboardStop: {
      description: 'Остановка дашборда',
      params: {
        dashboard: 'string - Идентификатор дашборда'
      }
    },
    dashboardErrors: {
      description: 'Получение критических ошибок',
      params: {
        dashboard: 'string - Идентификатор дашборда'
      }
    },
    dashboardErrorsClear: {
      description: 'Удаление всех критических ошибок',
      params: {
        dashboard: 'string - Идентификатор дашборда'
      }
    },
    dashboardListUpdate: {
      description: 'Обновление списка дашбордов',
      params: {}
    },
    dashboardList: {
      description: 'Получение списка дашбордов',
      params: {}
    },
    dashboardDeviceAction: {
      description: 'Отправка команды устройству',
      params: {
        dashboard: 'string - Идентификатор дашборда',
        device: 'string - Идентификатор устройства',
        action: 'string - Action устройства',
        push: 'object - Данные для отправки устройству'
      }
    },
    dashboard: {
      description: 'Получение данных дашборда',
      params: {
        dashboard: 'string - Идентификатор дашборда',      
      }
    },
    dashboardMeta: {
      description: 'Получение мета-данных дашборда',
      params: {
        dashboard: 'string - Идентификатор дашборда',      
      }
    },
    dashboardStructure: {
      description: 'Получение структуры дашборда',
      params: {
        dashboard: 'string - Идентификатор дашборда',      
      }
    },
  },

  async apiKeyAuth (key) {
    var result = await this.commandPromise('apiKeyAuth', { data: { key: key } })
    if (result.resultData.cipher) {
      result = await this.commandPromise('apiPrivateAuth', {
        data: { verify: this.cipherData(result.resultData.verify).toString() }
      })
    }
    this.level = result.resultData.level
    this.cipher = result.resultData.cipher
    this.authorized = true
    return result
  },

  promiseConnect(){
    return new Promise((resolve,reject)=>{
      this.open = resolve
      this.close = reject
      this.connect()
    })
  },

  connect(){
    if (this.connected === true) this.disconnect()
    this.disconnected = false
    this.connection = true
    this._ws = new WebSocket(this.address)

    this._ws.on('open', () => {
      this.connected = true
      this.connection = false
      this.open()
    })

    this._ws.on('close', () => {
      this.connected = false
      this.connection = false
      this.close(new Error("Error to connect: " + this.address))
      this._ws = null
    })

    this._ws.on('error', () => {
      this.connected = false
      this.connection = false
      this.close(new Error("Error to connect: " + this.address))
      this._ws = null
    })

    this._ws.on('message', (evt) => {
      var data = evt
      if (this.cipher) data = this.decipherData(data)
      const remoteData = JSON.parse(data)
      if (remoteData._pkgIndex) {
        if (this._queue.has(remoteData._pkgIndex)) {
          clearTimeout(this._queueTimeout.get(remoteData._pkgIndex))
          var func = this._queue.get(remoteData._pkgIndex)
          if (remoteData.result === 'error') {
            func.reject(new Error(remoteData.resultData))
          } else {
            func.resolve(remoteData)
          }
          this._queue.delete(remoteData._pkgIndex)
          this._queueTimeout.delete(remoteData._pkgIndex)
        }
      } else if (remoteData.command === 'broadcast') {
        if (this.channels.has(remoteData.target)) {
          this.channels.get(remoteData.target)(remoteData)
        }
      }
    })
  },

  setAddress (address) { this.address = address },
  setKey(key) { this.key = key },


  /**
   * Шифрование данных клиента
   *
   * @param {Object} client Данные клиента (см. getClient)
   * @param {String} data JSON данные
  */
  cipherData (data) {
    const cipher = crypto.createCipheriv('aes-256-cbc', this.private, this.key)
    cipher.setEncoding('utf8')
    let encrypted = ''
    encrypted += cipher.update(data, 'utf8', 'base64')
    encrypted += cipher.final('base64')
    return encrypted
  },
  
  /**
   * Дешифрование данных клиента
   *
   * @param {Object} client Данные клиента (см. getClient)
   * @param {String} data Base64 Данные
  */
  decipherData (data) {
    const cipher = crypto.createDecipheriv('aes-256-cbc', this.private, this.key)
    cipher.setEncoding('base64')
    let decrypted = ''
    decrypted += cipher.update(data, 'base64')
    decrypted += cipher.final('utf8')
    return decrypted
  },

  disconnect () {
    this._disconnect = true
    this.authorized = false
    if (this._ws) this._ws.close()
  },

  commandPromise (command, params) {
    return new Promise((resolve, reject) => {
      this.command(command, params, resolve, reject)
    })
  },

  command (command, params, resolve, reject) {
    if (params === undefined) params = {}
    params.command = command
    params._pkgIndex = this._pkgIndex
    this._pkgIndex++
    this.addToQueue(params, resolve, reject)
  },

  addToQueue (params, resolve, reject) {
    this._queue.set(params._pkgIndex, { resolve: resolve, reject: reject })
    this._queueTimeout.set(params._pkgIndex, setTimeout(() => {
      reject('Timeout')
      this._queue.delete(params._pkgIndex)
      this._queueTimeout.delete(params._pkgIndex)
    }, 5000))

    if (this.connected) {
      var data = JSON.stringify(params)
      if (this.cipher) data = this.cipherData(data)
      this._ws.send(data)
    } else {
      reject('WebSocket is close')
    }
  }
}